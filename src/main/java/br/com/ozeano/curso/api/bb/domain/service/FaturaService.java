package br.com.ozeano.curso.api.bb.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.domain.repository.FaturaRepository;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

@Service
public class FaturaService {

	@Autowired
	private FaturaRepository repository;
	
	public CobrancaInput transformarFaturaEmCobranca(Long faturaId) {
		var fatura = repository.getOne(faturaId);
		
		return criar(fatura);
	}
	
	public CobrancaInput criar(Fatura fatura) {
		
		var builder = CobrancaInput.builder();
		
		builder.numeroConvenio(Long.valueOf(fatura.getConvenio().getNumeroContrato()));
		builder.numeroCarteira(Integer.valueOf(fatura.getConvenio().getCarteira()));
		builder.numeroVariacaoCarteira(Integer.valueOf(fatura.getConvenio().getVariacaoCarteira()));
		
		return builder.build();
	}
}
